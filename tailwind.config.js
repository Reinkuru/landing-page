/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./application/views/*.php",
    "./application/views/**/*.php",
    "./application/views/**/**/*.php",
    "./application/views/**/**/**/*.php",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  plugins: [],
}
