// webpack.mix.js

let mix = require('laravel-mix');

mix.js('assets/js/app.js', 'public/js').postCss("assets/css/app.css", "public/css", [
    require("tailwindcss"),
    require("autoprefixer"),
]);