<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('_partials/head.php'); ?>
</head>

<body class="bg-[#F8F8F8]">
    <?php $this->load->view('_partials/navbar.php'); ?>
    <header class="bg-[#FFFFFF] w-full">
        <div class="container mx-auto">
            <img src="../assets/img/img-header.svg" alt="">
            <div class="grid grid-cols-0 top-[21.5rem] ml-[41.5rem] w-[38.5rem] h-[30rem] mx-auto head-bg place-items-end absolute">
                <h1 class="text-4xl p-8 mx-12 text-white text-justify leading-relaxed font-semibold">Belajar programming
                    lebih mudah dengan <mark style="background: #ffffff;">metode
                        studi kasus.</mark></h1>
                <p class="mr-[5.2rem] ml-[5.2rem] mb-5 text-white text-xl text-justify leading-relaxed font-light">Mudah
                    mengerti syntax dan paham membuat program secara utuh.</p>
                <button type="button" class="btn text-sm mb-12 font-bold mr-[21.4rem] mt-8 w-48 h-14 text-white bg-[#5F6CDE] hover:bg-[#505CBF] place-items-center">Daftar
                    sekarang!
                </button>
            </div>
        </div>
    </header>
    <section class="h-full w-full">
        <div class="container mx-auto m-44">
            <div class="grid gap-x-8 gap-y-4 grid-cols-3 h-[832px]">
                <div class="layout-1 rounded-lg">
                    <img class="h-[716px]" src="../assets/img/section1-learntocode.svg" alt="">
                    <div class="inner-layout-1 rounded-lg h-[112px]">
                        <div class="p-8">
                            <h1 class="text-2xl font-semibold text-white">Learn To Code</h1>
                            <p class="text-white font-normal text-base">Lebih dari 300+ kursus.</p>
                        </div>
                    </div>
                </div>
                <div class="grid-cols-1">
                    <div class="layout-1 rounded-lg">
                        <img class="h-[256px]" src="../assets/img/section1-frontend.svg" alt="">
                        <div class="inner-layout-1 rounded-lg h-[112px]">
                            <div class="p-8">
                                <h1 class="text-2xl font-semibold text-white">Frontend</h1>
                                <p class="text-white font-normal text-base">Lebih dari 200+ kursus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="mt-[90px] layout-1 rounded-lg">
                        <img class="h-[256px]" src="../assets/img/section1-backend.svg" alt="">
                        <div class="inner-layout-1 rounded-lg h-[112px]">
                            <div class="p-8">
                                <h1 class="text-2xl font-semibold text-white">Frontend</h1>
                                <p class="text-white font-normal text-base">Lebih dari 100+ kursus.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layout-1 rounded-lg">
                    <img class="h-[716px]" src="../assets/img/section1-uiux.svg" alt="">
                    <div class="inner-layout-1 rounded-lg h-[112px]">
                        <div class="p-8">
                            <h1 class="text-2xl font-semibold text-white">UI / UX </h1>
                            <p class="text-white font-normal text-base">Lebih dari 420+ kursus.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="h-full w-full relative z-[1]">
        <div class="container mx-auto">
            <h1 class="text-center text-[#36394E] font-semibold text-4xl mb-24">Dapatkan semua yang ingin anda pelajari
            </h1>
            <div class="grid gap-x-8 gap-y-4 grid-cols-4 p-5 place-items-center section2">
                <div class="text-center p-8" style="text-align: -webkit-center;">
                    <div class="h-[75px] w-[75px] bg-[#D9D9D9]">
                        <img class="top-3 relative" src="../assets/img/icons/tag.svg" alt="">
                    </div>
                    <h1 class="font-semibold text-lg p-2">4000+ kursus</h1>
                    <p class="text-base text-[#909090] p-2">Lorem ipsum dolor sit ametconsetetur sadipscing nonumy
                        eirmod</p>
                </div>
                <div class="text-center p-8" style="text-align: -webkit-center;">
                    <div class="h-[75px] w-[75px] bg-[#D9D9D9]">
                        <img class="top-3 relative" src="../assets/img/icons/user.svg" alt="">
                    </div>
                    <h1 class="font-semibold text-lg p-2">Instruktur ahli</h1>
                    <p class="text-base text-[#909090] p-2">Lorem ipsum dolor sit ametconsetetur sadipscing nonumy
                        eirmod</p>
                </div>
                <div class="text-center p-8" style="text-align: -webkit-center;">
                    <div class="h-[75px] w-[75px] bg-[#D9D9D9]">
                        <img class="top-3 relative" src="../assets/img/icons/zoom.svg" alt="">
                    </div>
                    <h1 class="font-semibold text-lg p-2">Akses seumur hidup</h1>
                    <p class="text-base text-[#909090] p-2">Lorem ipsum dolor sit ametconsetetur sadipscing nonumy
                        eirmod</p>
                </div>
                <div class="text-center p-8" style="text-align: -webkit-center;">
                    <div class="h-[75px] w-[75px] bg-[#D9D9D9]">
                        <img class="top-3 relative" src="../assets/img/icons/programming.svg" alt="">
                    </div>
                    <h1 class="font-semibold text-lg p-2">Online learning</h1>
                    <p class="text-base text-[#909090] p-2">Lorem ipsum dolor sit ametconsetetur sadipscing nonumy
                        eirmod</p>
                </div>
            </div>
        </div>
    </section>
    <section class="grid gap-x-8 gap-y-4 grid-cols-2 bg-[#36394E] relative bottom-16 h-[890px]">
        <div class="section3">
            <img class="relative top-[27rem] left-[32rem]" src="../assets/img/icons/Video.svg" alt="">
        </div>
        <div class="text-4xl font-semibold top-44 text-center text-white relative">
            <h1>Lihat, Praktek, Sukses!</h1>
            <p class="text-justify text-base font-normal p-color p-12 ml-24">Materi yang diberikan berupa studi kasus
                dan bersamaan dengan dasar-dasar programming, sehingga pelajar benar-benar mengerti proses pembuatan
                aplikasi dari awal. <br><br>
                Pengetahuan yang kami berikan berdasarkan apa yang dibutuhkan oleh industri teknologi saat ini, sehingga
                pelajar yang telah menguasai materi yang kami berikan akan siap untuk mulai karir di dunia IT.
            </p>
            <button type="button" class="btn text-sm mb-12 font-bold mr-[21.4rem] mt-8 ml-36 w-48 h-14 text-white bg-[#5F6CDE] hover:bg-[#505CBF] place-items-center">Daftar
                sekarang!
            </button>
        </div>
    </section>
    <section class="h-full w-full bottom-16 h-28 relative">
        <div class="container mx-auto">
            <div class="grid gap-x-8 gap-y-4 grid-cols-2 relative z-[1]">
                <div class="relative" style="text-align: -webkit-center;">
                    <div class="bg-[#5F6CDE] rounded-[20px] h-[170px] w-[170px] relative bottom-24">
                    </div>
                    <h1 class="text-4xl font-semibold items-center">Prestasi kami</h1>
                    <p class="text-justify p-12 ml-[9.5rem]">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
                    <button type="button" class="btn text-sm mb-12 font-bold mr-[21.4rem] ml-48 w-48 h-14 text-white bg-[#5F6CDE] hover:bg-[#505CBF] place-items-center">Daftar
                        sekarang!
                    </button>
                </div>
                <div class="h-[497px] w-670px bg-[#F0F1F8] relative bottom-24 rounded-[20px]">
                    <div class="bg-[#5B7D99] rounded-[20px]" style="text-align: -webkit-center;">
                        <img src="../assets/img/bg-uiux.svg" alt="">
                    </div>
                    <div class="grid gap-x-8 gap-y-4 grid-cols-3 text-center p-12">
                        <div class="text-5xl font-bold">
                            <h1>2m+</h1>
                            <p class="text-lg font-semibold">Siswa belajar</p>
                        </div>
                        <div class="text-5xl font-bold">
                            <h1>4k+</h1>
                            <p class="text-lg font-semibold">Kursus online</p>
                        </div>
                        <div class="text-5xl font-bold">
                            <h1>2k+</h1>
                            <p class="text-lg font-semibold">Instruktur ahli</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="h-full w-full relative top-[27rem] bg-[#5B7D99]">
        <div id="default-carousel" class="relative" data-carousel="static">
            <!-- Carousel wrapper -->
            <div class="relative h-56 overflow-hidden rounded-lg md:h-96">
                <!-- Item 1 -->
                <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item="">
                    <span class="absolute text-2xl font-semibold text-white -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 sm:text-3xl dark:text-gray-800">
                    </span>
                    <img src="../assets/img/Aliv pose 18.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-[1546px] w-[21rem] top-[14rem] left-[59rem]" alt="...">
                    <div class="relative text-item-carousel h-[229px] w-[355.85px] top-[6rem] left-[18rem]">
                        <img class="top-[2rem] right-[2rem] h-[60px] relative" src="../assets/img/icons/petik.svg" alt="">
                        <div class="text-white mr-4 ml-12 bottom-8 relative">
                            <p class="font-light text-sm mb-5">“Saya khusus mendedikasikan waktu
                                saya untuk belajar ngoding. Di R-Glasses belajarnya step by step, library-nya
                                up-to-date. Kalau ada eror, nggak bingung. Di sini saya juga belajar untuk nggak asal
                                coding. CV pun jadi bagus. Saya jadi percaya diri.”</p>
                            <h1 class="text-sm font-semibold">(Nama pengguna)</h1>
                            <h2 class="font-light text-xs">(Kelas yang diambil)</h2>
                        </div>
                    </div>
                </div>
                <!-- Item 2 -->
                <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-full z-10" data-carousel-item="">
                    <img src="../assets/img/Aliv pose 18.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-[1546px] w-[21rem] top-[14rem] left-[59rem]" alt="...">
                    <div class="relative text-item-carousel h-[229px] w-[355.85px] top-[6rem] left-[18rem]">
                        <img class="top-[2rem] right-[2rem] h-[60px] relative" src="../assets/img/icons/petik.svg" alt="">
                        <div class="text-white mr-4 ml-12 bottom-8 relative">
                            <p class="font-light text-sm mb-5">“Saya khusus mendedikasikan waktu
                                saya untuk belajar ngoding. Di R-Glasses belajarnya step by step, library-nya
                                up-to-date. Kalau ada eror, nggak bingung. Di sini saya juga belajar untuk nggak asal
                                coding. CV pun jadi bagus. Saya jadi percaya diri.”</p>
                            <h1 class="text-sm font-semibold">(Nama pengguna)</h1>
                            <h2 class="font-light text-xs">(Kelas yang diambil)</h2>
                        </div>
                    </div>
                </div>
                <!-- Item 3 -->
                <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-full z-10" data-carousel-item="">
                    <img src="../assets/img/Aliv pose 18.svg" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-[1546px] w-[21rem] top-[14rem] left-[59rem]" alt="...">
                    <div class="relative text-item-carousel h-[229px] w-[355.85px] top-[6rem] left-[18rem]">
                        <img class="top-[2rem] right-[2rem] h-[60px] relative" src="../assets/img/icons/petik.svg" alt="">
                        <div class="text-white mr-4 ml-12 bottom-8 relative">
                            <p class="font-light text-sm mb-5">“Saya khusus mendedikasikan waktu
                                saya untuk belajar ngoding. Di R-Glasses belajarnya step by step, library-nya
                                up-to-date. Kalau ada eror, nggak bingung. Di sini saya juga belajar untuk nggak asal
                                coding. CV pun jadi bagus. Saya jadi percaya diri.”</p>
                            <h1 class="text-sm font-semibold">(Nama pengguna)</h1>
                            <h2 class="font-light text-xs">(Kelas yang diambil)</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider indicators -->
            <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
            </div>
            <!-- Slider controls -->
            <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                <span class="inline-flex items-center justify-center w-8 h-8 rounded-2xl sm:w-10 sm:h-10 bg-white group-focus:ring-4 group-focus:ring-white group-focus:outline-none">
                    <svg width="18" height="28" viewBox="0 0 18 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M17.9567 24.71L7.27008 14L17.9567 3.29L14.6667 0L0.666748 14L14.6667 28L17.9567 24.71Z" fill="#6E38F7" />
                    </svg>
                    <span class="sr-only">Previous</span>
                </span>
            </button>
            <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                <span class="inline-flex items-center justify-center w-8 h-8 rounded-2xl sm:w-10 sm:h-10 bg-white group-focus:ring-4 group-focus:ring-white group-focus:outline-none">
                    <svg width="18" height="28" viewBox="0 0 18 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.666748 24.71L11.3534 14L0.666748 3.29L3.95675 0L17.9567 14L3.95675 28L0.666748 24.71Z" fill="#6E38F7" />
                    </svg>
                    <span class="sr-only">Next</span>
                </span>
            </button>
        </div>
    </section>
    <section class="container mx-auto relative top-[32rem]">
        <p class="text-center font-light">Apa yang ditunggu?</p>
        <h1 class="text-3xl text-[#36394E] text-center font-semibold mr-[22rem] ml-[22rem] p-8">Bergabunglah dengan
            karir membangun jaringan kami dengan ribuan pakar industri</h1>
        <button type="button" class="btn text-sm mb-12 font-bold mr-[21.4rem] ml-[34rem] w-48 h-14 text-white bg-[#5F6CDE] hover:bg-[#505CBF] place-items-center">Daftar
            sekarang!
        </button>
    </section>
    <?php $this->load->view('_partials/footer.php'); ?>
</body>
<script src="../assets/js/main.js"></script>

</html>