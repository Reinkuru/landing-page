<nav class="bg-[#36394E] px-2 w-full">
    <div class="container flex flex-wrap justify-between items-center mx-auto">
        <a href="https://reinkuru.github.io" class="flex items-center">
            <img src="../assets/img/logo-light.png" class="logo" alt="Rai Logo">
        </a>
        <div class="flex gap-3 md:order-2">
            <a href="<?= site_url('login') ?>"><button type="button" class="btn text-white bg-[#5F6CDE] hover:bg-[#505CBF] text-sm text-center">Login
                </button></a>
            <button type="button" class="btn signup text-white bg-[#FF6476] hover:bg-[#E8253C] text-sm text-center">Sign up
            </button>
        </div>
        <div class="justify-between items-center w-full flex w-auto">
            <ul class="flex p-4 mt-4 space-x-8 mt-0 text-sm border-0 uppercase">
                <li>
                    <a href="<?= site_url('beranda') ?>" class="block py-2 md:p-0 text-gradient" aria-current="">beranda</a>
                </li>
                <li>
                    <a href="#" class="block py-2 md:p-0 text-gradient">kursus</a>
                </li>
                <li>
                    <a href="#" class="block py-2 md:p-0 text-gradient">testimoni</a>
                </li>
            </ul>
        </div>
    </div>
</nav>