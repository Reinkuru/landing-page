<footer class="h-1 w-full relative top-[36rem]">
    <div class="container mx-auto bg-[#5F6CDE] h-[125px] w-[1170px] rounded-2xl relative z-[1]">
        <div class="flex flex-wrap space-x-20 items-center justify-center top-12 relative">
            <div class="facebook">
                <a href="http://">
                    <svg class="svg-menu" width="22" height="40" viewBox="0 0 22 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g class="svg-menu_facebook" cursor="pointer" pointer-events="all">
                            <g class="svg-menu_facebook_background">
                                <path d="M20.0195 22.5L21.1305 15.2609H14.1844V10.5633C14.1844 8.58281 15.1547 6.65234 18.2656 6.65234H21.4234V0.489063C21.4234 0.489063 18.5578 0 15.818 0C10.0977 0 6.35859 3.46719 6.35859 9.74375V15.2609H0V22.5H6.35859V40H14.1844V22.5H20.0195Z" fill="#F0F1F8" />
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="twitter">
                <a href="http://">
                    <svg class="svg-menu" width="40" height="32" viewBox="0 0 40 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g class="svg-menu_facebook" cursor="pointer" pointer-events="all">
                            <g class="svg-menu_facebook_background">
                                <path d="M35.35 7.97497C35.375 8.32495 35.375 8.67501 35.375 9.025C35.375 19.6999 27.2501 32 12.4 32C7.82499 32 3.57502 30.6749 0 28.375C0.650024 28.45 1.27496 28.475 1.94999 28.475C5.72494 28.475 9.19999 27.2 11.975 25.025C8.42499 24.95 5.44998 22.625 4.42496 19.425C4.92501 19.4999 5.42497 19.55 5.95002 19.55C6.675 19.55 7.40005 19.4499 8.07501 19.275C4.37502 18.5249 1.59993 15.275 1.59993 11.35V11.25C2.67489 11.85 3.925 12.225 5.2499 12.275C3.0749 10.8249 1.64995 8.34996 1.64995 5.54994C1.64995 4.04997 2.04988 2.67497 2.74992 1.47496C6.72494 6.37496 12.7 9.57491 19.3999 9.92496C19.2749 9.32496 19.1999 8.70002 19.1999 8.07501C19.1999 3.62496 22.7999 0 27.2749 0C29.5999 0 31.6999 0.974998 33.1749 2.55C34.9999 2.20002 36.7499 1.52498 38.2999 0.600004C37.6999 2.47505 36.4249 4.05005 34.7499 5.04998C36.3749 4.87506 37.9499 4.42496 39.3999 3.80003C38.3001 5.39996 36.925 6.8249 35.35 7.97497Z" fill="#F0F1F8" />
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="instagram">
                <a href="http://">
                    <svg class="svg-menu" width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g class="svg-menu_instagram" cursor="pointer" pointer-events="all">
                            <g class="svg-menu_instagram_background">
                                <path d="M18.675 9.09487C13.3768 9.09487 9.1032 13.3684 9.1032 18.6667C9.1032 23.9649 13.3768 28.2385 18.675 28.2385C23.9732 28.2385 28.2468 23.9649 28.2468 18.6667C28.2468 13.3684 23.9732 9.09487 18.675 9.09487ZM18.675 24.8896C15.2511 24.8896 12.4521 22.0989 12.4521 18.6667C12.4521 15.2345 15.2428 12.4438 18.675 12.4438C22.1072 12.4438 24.8979 15.2345 24.8979 18.6667C24.8979 22.0989 22.0988 24.8896 18.675 24.8896ZM30.8709 8.70334C30.8709 9.94459 29.8712 10.9359 28.6383 10.9359C27.3971 10.9359 26.4057 9.93626 26.4057 8.70334C26.4057 7.47042 27.4054 6.47075 28.6383 6.47075C29.8712 6.47075 30.8709 7.47042 30.8709 8.70334ZM37.2105 10.9692C37.0688 7.97858 36.3857 5.32947 34.1948 3.14686C32.0122 0.96426 29.3631 0.281156 26.3724 0.131206C23.2901 -0.0437354 14.0515 -0.0437354 10.9692 0.131206C7.98691 0.272825 5.3378 0.95593 3.14686 3.13853C0.95593 5.32114 0.281156 7.97025 0.131206 10.9609C-0.0437354 14.0432 -0.0437354 23.2818 0.131206 26.3641C0.272825 29.3548 0.95593 32.0039 3.14686 34.1865C5.3378 36.3691 7.97858 37.0522 10.9692 37.2021C14.0515 37.3771 23.2901 37.3771 26.3724 37.2021C29.3631 37.0605 32.0122 36.3774 34.1948 34.1865C36.3774 32.0039 37.0605 29.3548 37.2105 26.3641C37.3854 23.2818 37.3854 14.0515 37.2105 10.9692ZM33.2285 29.6713C32.5787 31.3041 31.3208 32.562 29.6796 33.2201C27.2221 34.1948 21.3908 33.9699 18.675 33.9699C15.9592 33.9699 10.1195 34.1865 7.67035 33.2201C6.03756 32.5703 4.77965 31.3124 4.12154 29.6713C3.14686 27.2138 3.37179 21.3824 3.37179 18.6667C3.37179 15.9509 3.15519 10.1112 4.12154 7.66202C4.77132 6.02923 6.02923 4.77132 7.67035 4.11321C10.1279 3.13853 15.9592 3.36346 18.675 3.36346C21.3908 3.36346 27.2305 3.14686 29.6796 4.11321C31.3124 4.76299 32.5703 6.0209 33.2285 7.66202C34.2031 10.1195 33.9782 15.9509 33.9782 18.6667C33.9782 21.3824 34.2031 27.2221 33.2285 29.6713Z" fill="#F0F1F8" />
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="youtube">
                <a href="http://">
                    <svg class="svg-menu" width="42" height="30" viewBox="0 0 42 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g class="svg-menu_youtube" cursor="pointer" pointer-events="all">
                            <g class="svg-menu_youtube_background">
                                <path d="M40.6002 4.56196C40.1233 2.76627 38.7182 1.35204 36.9341 0.872105C33.7003 0 20.7333 0 20.7333 0C20.7333 0 7.76641 0 4.53258 0.872105C2.7485 1.35212 1.34339 2.76627 0.866486 4.56196C0 7.81675 0 14.6076 0 14.6076C0 14.6076 0 21.3984 0.866486 24.6532C1.34339 26.4489 2.7485 27.8042 4.53258 28.2841C7.76641 29.1562 20.7333 29.1562 20.7333 29.1562C20.7333 29.1562 33.7003 29.1562 36.9341 28.2841C38.7182 27.8042 40.1233 26.4489 40.6002 24.6532C41.4667 21.3984 41.4667 14.6076 41.4667 14.6076C41.4667 14.6076 41.4667 7.81675 40.6002 4.56196ZM16.4924 20.7731V8.44201L27.3302 14.6077L16.4924 20.7731Z" fill="#F0F1F8" />
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="h-[38rem] w-full bg-[#36394E] relative bottom-[4rem]">
        <div class="container mx-auto relative top-[10rem]">
            <div class="grid gap-x-8 gap-y-4 grid-cols-4 place-items-center mx-auto ml-44 mr-48">
                <div class="">
                    <h1 class="text-white text-[18px] font-semibold">Pembelajaran</h1>
                    <div class="text-footer font-normal text-[15px]">
                        <ul class="mt-4">
                            <li>
                                <a class="hover:text-white" href="#">Tutorial</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Video kursus</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Instruktur</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Contoh</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="">
                    <h1 class="text-white text-[18px] font-semibold">Tentang kami</h1>
                    <div class="text-footer font-normal text-[15px]">
                        <ul class="mt-4">
                            <li>
                                <a class="hover:text-white" href="#">Tentang kami</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Karir</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Instruktur</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Iklan</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="">
                    <h1 class="text-white text-[18px] font-semibold">Bantuan</h1>
                    <div class="text-footer font-normal text-[15px]">
                        <ul class="mt-4">
                            <li>
                                <a class="hover:text-white" href="#">FAQ</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Pusat bantuan</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Hubungi kami</a>
                            </li>
                            <li>
                                <a class="hover:text-white" href="#">Masukan</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="">
                    <img src="<?php base_url(); ?>assets/img/logo-light.png" alt="">
                    <ul class="text-white">
                        <li class="font-semibold text-[18px]">Alamat perusahaan</li>
                        <li class="font-normal text-[15px] mt-6">Email us on</li>
                        <li class="font-semibold text-[18px]">email@namaperusahaan.com</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="p-20">
            <div class="container mx-auto relative top-[10rem]" style="text-align: -webkit-center;">
                <svg width="970" height="1" viewBox="0 0 970 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <line x1="4.37114e-08" y1="0.5" x2="970" y2="0.500085" stroke="white" stroke-opacity="0.15" />
                </svg>
                <div class="grid grid-cols-1 gap-x-8 gap-y-4 place-items-center mx-auto ml-44 mr-48 mt-5">
                    <div class="text-white">
                        <p class="font-normal text-[15px] text-right">Privacy Policy | Terms and conditions</p>
                    </div>
                    <div class="text-white">
                        <p class="font-normal text-[15px]">Created with heart♥ © 2022</p>
                    </div>
                </div>
            </div>
        </div>
</footer>