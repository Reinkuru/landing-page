<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Beranda extends CI_Controller
{
    public function index()
    {
        $data['meta'] = [
            'title' => 'Landing Page | Beranda',
        ];
        $this->load->view('beranda', $data);
    }
}