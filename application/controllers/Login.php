<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function index()
    {
        $data['meta'] = [
            'title' => 'Landing Page | Login',
        ];
        $this->load->view('auth/login', $data);
    }
}